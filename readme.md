
			     ALARM CLOCK
			     ===========

---- DATA STRUCTURES ----


sturct thread:
+	int64_t sleep_ticks;
+	struct semaphore sleep_sema;

sleep_ticks is the number of ticks the blocked thread need to wait before it wake up.
sleep_sema is a semaphore initialize to 0, it is used in timer_sleep.

---- ALGORITHMS ----

In timer_sleep(), we first assign ticks to sleep_ticks, then we call sema_down.
In timer interrupt we call check_timer_sleep for every thread, if sleep_tick == 0, we call sema_up


check_timer_sleep return immediately if the thread is not in BLOCK state. As long 
as the number of thread is not too huge, our design is efficient.

---- SYNCHRONIZATION ----

Each thread has their own sleep_ticks and sleep_sema. When multiple threads call 
timer_sleep(), they do not affect each other.


When we call sema_down, it takes care of the race conditions. Inside sema_down,
the interrupt is off in the line: old_level = intr_disable ();

---- RATIONALE ----

This design is easy to implement and understand. We don't have to block and unblock a thread ourselves,
the semaphore takes care of the low level operations and solve the synchronization problem.
We don't need to create extra data structure in timer.c, thread keep all information 
they need for timer_sleep.

			 PRIORITY SCHEDULING
			 ===================

---- DATA STRUCTURES ----


sturct thread:
+	int old_priority;
+	struct lock hl;
+	struct lock wl;

old_priority holds the priority value when donation occur.
hl is the lock the thread is holding.
wl is the lock the thread is waiting.

struct lock:
+	struct priority;
+	struct list_elem donor_list

priority is the highest priority of threads waiting for this lock.
donor_list is the record of donors.


donor_list is used to track priority donation. It is a list of thread that are 
waiting, we can sort the list according to priority.

lock Lock1, Lock2;
thread High, Medium, Low;

Low acquire Lock1;
Medium acquire Lock2;
Medium try to acquire Lock1; (failed)
Medium donate priority to Low; (Low->donor_list: Medium)
High try to acquire Lock2; (failed)
High donate priority to Medium; (Medium->donor_list High)
High donate priority to Low; (Low->donor_list: Medium, High)
Low release Lock1;
Low lower its priority;
Medium release Lock2;
Medium lower its priority;

---- ALGORITHMS ----


Currently, these synchronization primitive just wake up the thread in front of
wait list, we can simply sort the list based on the priority, then lock, semaphore,
and condition variable will always wake up the one with highest priority.


When a thread try to acquire a lock that is hold by a lower priority thread, 
increase the priority of the lower priority thread. After the higher priority thread 
acquire the lock, cancel the donation.

For nestled donation, the thread receive the donation can keep a record of 
the donors and their priority, then choose the highest one.


The thread should lower its priority to original if there is only one donor,
if there are multiple donors, lower its priority to the second highest. So as
mentioned above, we need a data structure to record all donors.

---- SYNCHRONIZATION ----


thread_set_priority() can be called from multiple places, if it happens simultaneously,
there will be race condition. We can implement a semaphore to avoid, initialize the
semaphore to 1, only one thread at time can run thread_set_priority().
We can use lock instead of semaphore, since the thread acquired the lock can release the 
lock at the end of thread_set_priority().

---- RATIONALE ----


In this design, we used the helper function in list.h, we can sort the ready_list and
synchronization primitives' wait list easily. For priority donation, we clearly divide 
the job between lock_acquire and lock_release, which can better handle nestled donation and 
multiple donation.

			  ADVANCED SCHEDULER
			  ==================

---- DATA STRUCTURES ----


struct thread:
+	int nice;
+	int recent_cpu;

nice is the nice value for the thread.
recent_cup is the the recent CUP for the thread.

Thread.c:
+	int load_avg;

load_avg is the load average for the system.


---- ALGORITHMS ----


When two thread have same priority, the table is uncertain.
The rule we used is choose the one with lowest tid value, which we 
assume A'tid < B'tid < C'tid.
This does not match our scheduler, our scheduler uses list_insert_ordered
to sort threads, when two thread have equal priority, scheduler choose the one 
in the front.

We basically put all computation inside the timer_interrupt, since we have
to update recent_cpu and load_avg every second, we also need to update priority
every 4 ticks, we can only put them inside timer_interrupt. 
Computation that not relate to ticks can be put outside, for example, set nice value,
set priority.

---- RATIONALE ----


Advantages: the logic is sample, we just update recent_cpu, load_avg, and priority
periodically, the default scheduler takes care to everything else.
Disadvantages: most of computation are done inside timer_interrupt, which is not
good for performance.

If we have more time, we might implement a more detailed version of MLFQ,
we should have a quantum for each level, if the higher level job run out of its 
quantum, it should have a chance to run in lower level. 
As for the way we calculate priority, we can use a simpler formula, if a thread 
is Blocked before the timer interrupt, increase its priority, if a thread always 
run out its quantum, decrease its priority. This formula gives higher priority to 
I/O intensive threads,and lower priority to CPU intensive threads.


Currently, PintOS dose not support float type computation, we think it is a 
good idea to implement a new data type, fiexed-point, we could use this type in 
future PintOS projects. It is a good programming practice to write reusable code.

We create an abstract data type and a set of functions associate with the type.
With this complete abstraction, we can hide low level details from programmers.

Second design we might implement: since there are only few formula we have to compute, 
we can do arithmetic operation without any abstraction. We can manually shift bit and take
the integer part. Although not a good design, but its sufficient for this project.

